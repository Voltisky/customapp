<?php
/**
 * Created by PhpStorm.
 * User: Karol Włodek
 * Date: 28.10.2017
 * Time: 21:15
 */

namespace Backend\UserBundle\Repository;


use Doctrine\ORM\EntityRepository;

class User extends EntityRepository {
	/**
	 * Get users list
	 * @return array
	 * @author Karol Włodek
	 */
	public function findAllUsers() {
		$em = $this->getEntityManager();

		$query = $em->createQueryBuilder( 'u' )
		            ->select( 'PARTIAL u.{id, username, firstname, lastname}' )
		            ->from( \Backend\UserBundle\Entity\User::class, 'u' )
		            ->getQuery();

		return $query->getResult();
	}
}