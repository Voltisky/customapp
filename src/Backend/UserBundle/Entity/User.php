<?php

namespace Backend\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;


/**
 * User
 */
class User implements UserInterface, \JsonSerializable
{
	/**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $is_active = true;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt = null)
    {
	    if(!$createdAt)
	    {
		    $createdAt = new \DateTime();
	    }

        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt = null)
    {
    	if(!$updatedAt)
	    {
	        $updatedAt = new \DateTime();
	    }

        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
	/**
	 * @inheritDoc
	 */
	public function getRoles() {
		return array("ROLE_USER");
	}

	/**
	 * @inheritDoc
	 */
	public function getSalt() {
		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function eraseCredentials() {}

	public function onPrePersist(){
		$date = new \DateTime();
		$this->setCreatedAt($date);
		$this->setUpdatedAt($date);
	}
	public function onPreUpdate(){
		$date = new \DateTime();
		$this->setUpdatedAt($date);
	}

	/**
	 *
	 * @return string
	 * @author Karol Włodek
	 */
	public function getDisplay(){
		if($this->getFirstname() && $this->getLastname())
		{
			return sprintf("%s %s", $this->getFirstname(), $this->getLastname());
		}
		else
		{
			return $this->getUsername();
		}
	}

	/**
	 * @return array
	 * @author Karol Włodek
	 */
	public function jsonSerialize() {
		return [
			"id" => $this->id,
			"username" => $this->username,
			"email" => $this->email,
			"firstname" => $this->firstname,
			"lastname" => $this->lastname,
			"is_active" => $this->is_active
		];
	}
}
