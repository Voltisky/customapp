<?php
/**
 * Created by PhpStorm.
 * User: Karol Włodek
 * Date: 27.10.2017
 * Time: 22:11
 */

namespace Backend\UserBundle\Form;


use Backend\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\BadMethodCallException;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegisterType extends AbstractType {
	private $encoder;
	private $plainPassword;

	public function buildForm( FormBuilderInterface $builder, array $options ) {
		if(!$options["encoder"])
		{
			throw new BadMethodCallException("Encoder required.");
		}

		$this->encoder = $options["encoder"];

		$builder
			->add( 'username', TextType::class, [ "label" => "backend.form.user_register.field.username" ] )
			->add( 'email', EmailType::class, [ "label" => "backend.form.user_register.field.email" ] )
			->add( 'plainPassword', RepeatedType::class, [
				"type"           => PasswordType::class,
				"first_options"  => [
					"label" => "backend.form.user_register.field.password"
				],
				"second_options" => [
					"label" => "backend.form.user_register.field.repeat_password"
				],
				"mapped"         => false
			] )
			->add( 'firstname', TextType::class, [
				"label" => "backend.form.user_register.field.firstname"
			] )
			->add( 'lastname', TextType::class, [
				"label" => "backend.form.user_register.field.lastname"
			] )
		->add('submit', SubmitType::class, [
			"label" => "backend.form.user_register.field.submit"
		]);

		$builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
		$builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
	}

	/**
	 * Store plain password on pre set data
	 * @param FormEvent $event
	 *
	 * @author Karol Włodek
	 */
	public function onPreSubmit(FormEvent $event){
		$this->plainPassword = $event->getData()["plainPassword"]["first"];
	}

	/**
	 * Encode plain password to encryption specific for this user
	 * @param FormEvent $event
	 *
	 * @author Karol Włodek
	 */
	public function onPostSubmit(FormEvent $event){
		/** @var User $data */
		$data = $event->getData();

		$password = $this->encoder->encodePassword($data, $this->plainPassword);
		$data->setPassword($password);
	}

	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( array(
			'data_class' => User::class,
			'encoder' => null
		) );
	}

}