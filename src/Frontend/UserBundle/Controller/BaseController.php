<?php

namespace Frontend\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
	/**
	 * Check if is logged in
	 * @return bool
	 * @author Karol Włodek
	 */
	protected function isLogged() {
		return $this->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' );
	}

	/**
	 * Add session message with translation
	 *
	 * @param $type
	 * @param $message
	 *
	 * @author Karol Włodek
	 */
	protected function addMessage( $type, $message ) {
		$session = $this->get( "session" );
		$tr      = $this->get( 'translator' );
		$session->getFlashBag()->add( $type, $tr->trans( $message ) );
	}
}
