<?php

namespace Frontend\UserBundle\Controller;

use Backend\UserBundle\Repository\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RestController extends Controller {
	/**
	 * Collection with users
	 * Without implementation
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 * @author Karol Włodek
	 */
	public function listAction( Request $request ) {
		$repo = $this->getUserRepo();

		$users = $repo->findAllUsers();

		$serializer = $this->get( 'serializer' );
		$data       = $serializer->serialize( $users, 'json' );

		return new JsonResponse( $data );
	}

	/**
	 * Delete user from DB
	 *
	 * @param Request $request
	 *
	 * @return Response
	 * @author Karol Włodek
	 */
	public function deleteAction( Request $request ) {
		$tr   = $this->get( "translator" );
		$repo = $this->getUserRepo();

		$userId = $request->get( "user" );
		if ( ! $userId ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.bad_request" ) ], Response::HTTP_BAD_REQUEST );
		}

		$user = $repo->find( $userId );
		if ( $user->getId() == $this->getUser()->getId() ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.action.delete_same_user" ) ], Response::HTTP_BAD_REQUEST );
		}

		try {
			$em = $this->getDoctrine()->getManager();
			$em->remove( $user );
			$em->flush();
		} catch ( \Exception $e ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.action.delete.db" ) ] );
		}

		return new JsonResponse( [ "message" => $tr->trans( "frontend.success.action.delete" ) ] );
	}

	/**
	 * @param Request $request
	 * @param \Backend\UserBundle\Entity\User $user
	 *
	 * @return Response
	 * @author Karol Włodek
	 */
	public function editAction( Request $request, UserPasswordEncoderInterface $encoder ) {
		$tr = $this->get( "translator" );

		$data     = $request->get( "data", "" );
		$formData = null;
		try {
			$data = json_decode( $data, true );
		} catch ( \Exception $e ) {
			$data = null;
		}

		if ( ! $data ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.bad_request" ) ], Response::HTTP_BAD_REQUEST );
		}

		$resolver = new OptionsResolver();
		$formData = $resolver->setRequired( [
			"id",
			"username",
			"email",
			"password",
			"firstname",
			"lastname",
			"is_active"
		] )->resolve( $data );

		$em = $this->getDoctrine()->getManager();

		$repo = $em->getRepository( \Backend\UserBundle\Entity\User::class );

		try {
			$user = $repo->find( $formData["id"] );
			$user = $this->mergeUserData( $user, $encoder, $formData );

			$validator = $this->get( "validator" );

			$errors = $validator->validate( $user );
			if ( count( $errors ) ) {
				$message = "";
				foreach ( $errors as $error ) {
					$message = $error->getMessage() . "" . $message;
				}

				return new JsonResponse( [ "message" => $message ], Response::HTTP_BAD_REQUEST );
			}

			$em->persist( $user );
			$em->flush();
		} catch ( \Exception $e ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.bad_request" ) ], Response::HTTP_BAD_REQUEST );
		}

		return new JsonResponse( [ "message" => $tr->trans( "frontend.success.action.edit" ) ], Response::HTTP_OK );
	}

	/**
	 * @param Request $request
	 *
	 * @author Karol Włodek
	 */
	public function createAction( Request $request, UserPasswordEncoderInterface $encoder ) {
		$tr = $this->get( "translator" );

		$data     = $request->get( "data", "" );
		$formData = null;
		try {
			$data = json_decode( $data, true );
		} catch ( \Exception $e ) {
			$data = null;
		}

		if ( ! $data ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.bad_request" ) ], Response::HTTP_BAD_REQUEST );
		}

		$resolver = new OptionsResolver();
		$formData = $resolver->setRequired( [
			"username",
			"email",
			"password",
			"firstname",
			"lastname",
			"is_active"
		] )->resolve( $data );

		$em = $this->getDoctrine()->getManager();

		$user = new \Backend\UserBundle\Entity\User();

		try {
			$user = $this->mergeUserData( $user, $encoder, $formData );

			$validator = $this->get( "validator" );

			$errors = $validator->validate( $user );
			if ( count( $errors ) ) {
				$message = "";
				foreach ( $errors as $error ) {
					$message = $error->getMessage() . "" . $message;
				}

				return new JsonResponse( [ "message" => $message ], Response::HTTP_BAD_REQUEST );
			}

			$em->persist( $user );
			$em->flush();
		} catch ( \Exception $e ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.bad_request" ) ], Response::HTTP_BAD_REQUEST );
		}

		return new JsonResponse( [ "message" => $tr->trans( "frontend.success.action.create" ) ], Response::HTTP_OK );
	}

	/**
	 * Get data for specific user
	 *
	 * @param Request $request
	 * @param \Backend\UserBundle\Entity\User $user
	 *
	 * @return JsonResponse
	 * @author Karol Włodek
	 */
	public function getUserAction( Request $request ) {
		$id   = $request->get( "user" );
		$em   = $this->getDoctrine()->getManager();
		$repo = $em->getRepository( \Backend\UserBundle\Entity\User::class );
		$tr   = $this->get( "translator" );

		$user = null;
		try {
			$user = $repo->find( $id );
		} catch ( \Exception $e ) {
			return new JsonResponse( [ "message" => $tr->trans( "frontend.error.bad_data" ) ] );
		}

		return new JsonResponse( [ "data" => $user ] );
	}


	/**
	 * Get User class repository
	 * @return \Doctrine\Common\Persistence\ObjectRepository|User
	 * @author Karol Włodek
	 */
	private function getUserRepo() {
		$em = $this->getDoctrine()->getManager();

		return $em->getRepository( \Backend\UserBundle\Entity\User::class );
	}

	/**
	 * Prepare user with passed data
	 * Encode password
	 *
	 * @param \Backend\UserBundle\Entity\User $user
	 * @param UserPasswordEncoderInterface $encoder
	 * @param $data
	 *
	 * @return \Backend\UserBundle\Entity\User
	 * @author Karol Włodek
	 */
	private function mergeUserData( \Backend\UserBundle\Entity\User $user, UserPasswordEncoderInterface $encoder, $data ) {
		$user->setUsername( $data["username"] );
		$user->setEmail( $data["email"] );

		if ( $data["password"] && strlen( $data["password"] ) > 0 ) {
			$password = $encoder->encodePassword( $user, $data["password"] );

			$user->setPassword( $password );
		}

		$user->setFirstname( $data["firstname"] );
		$user->setLastname( $data["lastname"] );
		$user->setIsActive( $data["is_active"] );

		return $user;
	}
}
