<?php

namespace Frontend\UserBundle\Controller;

use Backend\UserBundle\Entity\User;
use Backend\UserBundle\Form\UserRegisterType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class DefaultController
 * @package Frontend\UserBundle\Controller
 */
class DefaultController extends BaseController {
	/**
	 * Default page for application
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @author Karol Włodek
	 */
	public function indexAction() {
		return $this->render( 'FrontendUserBundle:Default:index.html.twig' );
	}

	/**
	 * Login user view
	 *
	 * @param AuthenticationUtils $auth
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @author Karol Włodek
	 */
	public function loginAction( AuthenticationUtils $auth ) {
		if ( $this->isLogged() ) {
			$this->addMessage( "error", "frontend.error.already_logged" );

			return $this->redirectToRoute( "frontend_user_homepage" );
		}

		$tr = $this->get( 'translator' );

		$form = $this->getLoginForm();

		$error = $auth->getLastAuthenticationError();
		if ( $error ) {
			$this->addMessage( 'error', $tr->trans( $error->getMessageKey(), $error->getMessageData(), 'security' ) );
		}

		return $this->render( '@FrontendUser/Default/login.html.twig', [ "form" => $form->createView() ] );
	}

	/**
	 * Create user with registration form type
	 *
	 * @param Request $request
	 * @param UserPasswordEncoderInterface $encoder
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @author Karol Włodek
	 */
	public function registerAction( Request $request, UserPasswordEncoderInterface $encoder ) {
		if ( $this->isLogged() ) {
			$this->addMessage( "error", "frontend.error.already_logged" );

			return $this->redirectToRoute( "frontend_user_homepage" );
		}

		$user = new User();
		$em   = $this->getDoctrine()->getManager();

		$form = $this->getRegistrationForm( $encoder, $user );

		$form->handleRequest( $request );
		if ( $form->isSubmitted() ) {
			$isValid = $form->isValid();
			if ( $isValid ) {
				$em->persist( $user );
				$em->flush();

				$this->addMessage( "success", "backend.form.user_register.success" );
			} else {
				$this->addMessage( "error", "backend.form.user_register.fail" );
			}
		}

		return $this->render( "@FrontendUser/Default/register.html.twig", [
			"form" => $form->createView()
		] );
	}

	/**
	 * Assign registration form
	 *
	 * @param UserPasswordEncoderInterface $encoder
	 * @param User|null $user
	 *
	 * @return \Symfony\Component\Form\Form
	 * @author Karol Włodek
	 */
	private function getRegistrationForm( UserPasswordEncoderInterface $encoder, User $user = null ) {
		$form = $this->createForm( UserRegisterType::class, $user, [ "encoder" => $encoder ] );

		return $form;
	}

	/**
	 * Generate login form
	 * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
	 * @author Karol Włodek
	 */
	private function getLoginForm() {
		$form = $this->createFormBuilder()
		             ->setMethod( "POST" )
		             ->setAction( $this->generateUrl( 'frontend_user_login' ) )
		             ->add( '_username', TextType::class, [ "label" => "backend.form.user_register.field.username" ] )
		             ->add( '_password', PasswordType::class, [ "label" => "backend.form.user_register.field.password" ] )
		             ->add( 'submit', SubmitType::class, [ "label" => "backend.form.field.login" ] )
		             ->getForm();

		return $form;
	}

	public function listAction( Request $request ) {
		if ( ! $this->isLogged() ) {
			$this->addMessage( "error", "frontend.error.not_logged" );

			return $this->redirectToRoute( "frontend_user_homepage" );
		}

		$em = $this->getDoctrine()->getManager();
		$userRepo = $em->getRepository(User::class);

		$userList = $userRepo->findAll();

		return $this->render("@FrontendUser/CRUD/list.html.twig", [
			"users" => $userList,
			"form" => $this->getCustomUserForm()->createView()
		]);
	}

	/**
	 * Custom form for create new/edit user
	 * @param User|null $user
	 *
	 * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
	 * @author Karol Włodek
	 */
	private function getCustomUserForm( User $user = null ) {
		$form = $this->createFormBuilder( $user )
		             ->add( 'username', TextType::class, [ "label" => "backend.form.user_register.field.username" ] )
		             ->add( 'email', EmailType::class, [ "label" => "backend.form.user_register.field.email" ] )
		             ->add( 'password', PasswordType::class, [ "label" => "backend.form.user_register.field.password", "required" => false ] )
		             ->add( 'firstname', TextType::class, [ "label" => "backend.form.user_register.field.firstname" ] )
		             ->add( 'lastname', TextType::class, [ "label" => "backend.form.user_register.field.lastname" ] )
		             ->add( 'is_active', CheckboxType::class, [ "label" => "backend.form.user_register.field.isActive", "required" => false ] )
		;

		return $form->getForm();
	}
}
